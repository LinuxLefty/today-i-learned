# Referencing to a query alias

When you have a query where you rename a column or use an expression such as:

```sql
SELECT some_column AS some_alias FROM some_table WHERE some_alias >= 5;
```

or

```sql
SELECT some_column_1 - some_column_2 AS some_alias FROM some_table WHERE some_alias >= 5;
```

you'll see an error such as:

```
ERROR:  column "some_alias" does not exist
```

In order to solve this, you'll need to use a sub-query. For example:

```sql
SELECT * FROM (SELECT some_column AS some_alias FROM some_table) subquery WHERE subquery.some_alias >= 5;
SELECT * FROM (SELECT some_column_1 - some_column_2 AS some_alias FROM some_table) subquery WHERE subquery.some_alias >= 5;
```
