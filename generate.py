#!/bin/env python3

import datetime
import subprocess
import pathlib
import io


class Generator:
    def __init__(self, root: str):
        self._root = root

    def _dir(self, path: pathlib.Path) -> list:
        return sorted(path.iterdir())

    def _title(self, path: pathlib.Path) -> str:
        with open(path, 'r') as r:
            return r.readline().replace('#', '').strip().title()

    def _is_hidden(self, path: pathlib.Path) -> bool:
        final_path = path.parts[-1]
        ext = final_path.partition('.')[-1]

        if final_path[0] == '.':
            return True
        if ext not in ['', 'md']:
            return True

        return False

    def _walk(self, path: pathlib.Path, target: io.TextIOWrapper, level: int = 1) -> None:

        if self._is_hidden(path):
            return

        target.write(f'\n#{"#" * level} {path.parts[-1].title()}\n')

        for subpath in self._dir(path):

            if subpath.is_dir():
                self._walk(subpath, target, level + 1)
            elif not self._is_hidden(subpath):
                url = '/'.join(subpath.parts)
                title = self._title(subpath)

                target.write(f'* [{title}]({url})\n')

    def run(self, target: io.TextIOWrapper):
        target.write('# Today I Learned\n')

        for path in self._dir(pathlib.Path(self._root)):
            if path.is_dir():
                self._walk(path, target)


with open('README.md', 'w') as target:
    Generator('.').run(target)

with open('README.md') as target:
    print(target.read())

subprocess.check_call(['git', 'add', '.'])
subprocess.check_call(['git', 'commit', '-m', f'Added TIL for {datetime.datetime.now().isoformat()}'])
subprocess.check_call(['git', 'push'])
