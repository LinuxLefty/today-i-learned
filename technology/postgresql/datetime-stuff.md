# Time Related Things

## How to get the current datetime

```sql
SELECT NOW();
```

## How to find recent records:

```sql
SELECT * FROM table WHERE column_name > NOW() - INTERVAL '1 day'
```

## How to query `tsrange` columns:

* The first part can be queried using `LOWER()`
* The second part can be queried using `UPPER()`

For example, to find the duration

```sql
SELECT UPPER(column_name) - LOWER(column_name) AS duration FROM table;
```

## How to convert an interval to an integer

Use `EXTRACT(epoch FROM [interval])`. For example:

```sql
SELECT EXTRACT(epoch FROM my_interval);
```
