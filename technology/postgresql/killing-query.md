# Terminating an Ongoing Query

If you have a process identifier (`pid`) that needs to be terminated, use the following query:

```
SELECT pg_cancel_backend(<pid of the process>)
```

This will attempt to cancel the query in a "nice" way. This is similar to a `SIGTERM` in Unix

If this does not work, then use the following query:


```
SELECT pg_terminate_backend(<pid of the process>)
```

This this will kill the query and will not give it time to clean up. This is a `SIGKILL` in Unix.
