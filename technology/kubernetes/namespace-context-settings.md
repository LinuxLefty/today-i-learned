# Context and Namespace

## Context

To select a context, use `kubectx` without any arguments (or provide the desired context as an argument)

To get your context, use `kubectx --current`:

```
$> kubectx --current

engineering-prod_us-east1_prod
```

## Namespace

To select a namespace, use `kubens` without any arguments (or provide the desire namespace as an argument)

To get your current namespace, use `kubens --current`:

```
$> kubens --current

team-dri
```
