# Debugging "Container image xxx already present on machine" Error

If you have a pod that is stuck in `CrashLoopBackOff`, with the following message: `Container image xxx already present on machine`, then check the logs. This message is super misleading and it just means the pod is failing to start

```
$> kubectl describe pods/jager2-smoketest-cd4bb7474-xp4bb
[...snip...]
Events:
  Type     Reason   Age                       From                           Message
  ----     ------   ----                      ----                           -------
  Normal   Pulled   8m53s (x89 over 7h18m)    kubelet, gke-prod-primary-prod Container image "gcr.io/XXX:3d6fd89" already present on machine
  Warning  BackOff  3m54s (x2002 over 7h18m)  kubelet, gke-prod-primary-prod restarting failed container

$> kubectl logs XXX
(... some error here ...)
```
