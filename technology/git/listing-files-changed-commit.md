# Listing the Files Changed in a Commit

To list the files changed in a commit, use the following command:

```
git show --name-only [commit-id]
```

To do the same thing, but list it for script processing, use the following command:

```
git diff-tree --no-commit-id --name-only -r [commit-id]
```
