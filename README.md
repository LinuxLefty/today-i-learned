# Today I Learned

## Business

### Communication
* [Handbook-First Documentation](business/communication/handbook-first-documentation.md)

## Farm

### Cows
* [Cuts Of A Cow](farm/cows/cuts.md)

### Misc
* [How To Switch A House To Generator Power](farm/misc/how-to-start-generator.md)
* [Stock Tank Pools](farm/misc/stock-tank-pools.md)

## Technology

### Docker
* [How To Prune Unused Docker Objects](technology/docker/cleanup.md)

### Git
* [Listing Commits By Author](technology/git/listing-commits-by-author.md)
* [Listing The Files Changed In A Commit](technology/git/listing-files-changed-commit.md)

### Golang
* [How To Set Dependency To A Git Hash](technology/golang/setting-depend-version-git-hash.md)

### Kubernetes
* [Connecting To A Cloud Sql Database](technology/kubernetes/connecting-to-database.md)
* [Debugging "Container Image Xxx Already Present On Machine" Error](technology/kubernetes/container-image-already-present.md)
* [How To Insepect Things In Kubernetes](technology/kubernetes/inspecting-things.md)
* [Launching A Shell In Kubernetes](technology/kubernetes/launching-shell.md)
* [Context And Namespace](technology/kubernetes/namespace-context-settings.md)
* [Retrieving Kubernetes Secrets](technology/kubernetes/retrieving-secrets.md)

### Postgresql
* [Seeing What Queries Are Blocked](technology/postgresql/blocked-queries.md)
* [Time Related Things](technology/postgresql/datetime-stuff.md)
* [Terminating An Ongoing Query](technology/postgresql/killing-query.md)
* [Listing Information About Relations](technology/postgresql/listing-relation-info.md)
* [Referencing To A Query Alias](technology/postgresql/using_query_aliases.md)

### Pytest
* [How To Change The Execution Order](technology/pytest/change-execution-order.md)
