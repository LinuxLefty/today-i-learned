# Cuts of a Cow

These are the various cuts of a cow

## Front

* **Shoulder Steak** -- Lean. Good for grilling or skillets.
* **Chuck Roast** -- Good for roasts
* **Brisket Steak** -- Good for roasts
* **Rib Steak** -- Good for grilling
* **Chuck Steak** (top blade steak) -- Good for grilling or skillets
* **Delmonico Steak** -- Good for grilling or cast iron
* **Ribe-Eye Steak** -- Good for grilling or cast iron
* **Beef Ribs** (back ribs) -- Good for slow cooking
* **Skirt Steak** -- Good for grilling, skillet, or stir fry

## Rear

* **Round Roast** -- Lean. Good for roasts or slow cooking
* **Sirloin Tip Roast** (round tip roast) -- Lean. Good for roasts or slow cooking
* **Sirloin Steak** -- Lean. Good for grilling, skillets, or stir fry
* **Filet Mignon Steak** -- Lean. Good for grilling or skillet-to-oven
* **Porterhouse Steak** -- Good for grilling or skillet
* **Flank Steak** -- Lean. Good for grilling or stir-frys
* **T-Bone Steak** -- Lean. Grilling or cast iron
* **Oxtails** -- Good for slow cooking

## Best burger cuts

* Chuck roast is the best (80/20 fat)
* Sirloin is second best (90/10 fat)


## References:

* https://www.thespruceeats.com/what-is-a-delmonico-steak-995643
* https://www.finedininglovers.com/article/beef-cuts-explained-your-ultimate-guide-different-cuts-beef
* https://www.coolmomeats.com/2017/06/12/best-cuts-of-beef-for-burgers-tips-from-a-professional-butcher
