# How to Insepect Things in Kubernetes

To get information about a pod:

```
kubectl describe pods/[pod-identifier]
```
