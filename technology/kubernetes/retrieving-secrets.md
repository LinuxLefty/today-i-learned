# Retrieving Kubernetes Secrets

To retrieve a secret from Kubernetes, first you need to get the secret:

```
kubectl get secret [name] -o yaml
```

This will produce something like:

```
apiVersion: v1
data:
  sdkKey: c29tZS1jb29sLXNlY3JldAo=
kind: Secret
metadata:
  creationTimestamp: "2020-03-02T20:47:57Z"
  name: some-secret-key
  namespace: some-namespace
  ownerReferences:
  - apiVersion: bitnami.com/v1alpha1
    controller: true
    kind: SealedSecret
    name: some-secret-key
    uid: 000-0-0000-0-000
  resourceVersion: "12345"
  selfLink: /api/v1/namespaces/some-namespace/secrets/some-secret-key
  uid: 0000-00-0-00000-000
type: Opaque
```

`sdkKey` has the encoded value. To decode it, run it through `base64`:

```
$> echo "c29tZS1jb29sLXNlY3JldAo=" | base64 -d
some-cool-secret
```
