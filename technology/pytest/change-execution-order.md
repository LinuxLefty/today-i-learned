# How to Change the Execution Order

Instead of running the entire test every single time, it would be nice to just run the necessary ones, especially when developing new features or getting existing tests to pass.

Here are some useful flags I found out:

* `--lf, --last-failed` -- Rerun only the tests that failed at the last run (or all if none failed)
* `--ff, --failed-first` -- Run all tests but run the last failures first. This may re-order tests and thus lead to repeated fixture setup/teardown
* `--nf, --new-first` -- Run tests from new files first, then the rest of the tests sorted by file mtime

I have noticed a strange behavior in `--last-failed`. When it says `(or all if none failed)` that doesn't seem to pick up new tests until it is run without `--last-failed` (or at least when using `ptw`)
