# Connecting to a Cloud SQL Database

In order to connect to a Cloud SQL database, you'll want to use the `gcloud beta sql` command:

```
gcloud beta sql connect <database> --user <user> --database <database>
```

If you want to launch multiple shells, you'll run into problems as both shells with attempt to grab the same port. In this case, use a random port:

```
gcloud beta sql connect <database> --user <user> --database <database> --port $(random 2000 65000)
```
