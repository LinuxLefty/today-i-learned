How to Prune Unused Docker Objects
==================================

To prune everything (except volumes), use the following command:

```
docker system prune
```

This command takes a long time, so this might work a bit better:

```
docker image prune
docker container prune
```


See https://docs.docker.com/config/pruning/ to see a full list of things you can do
