# Listing Commits by Author

To list the commits by an author, use `git log`'s `--author` flag.

This accepts either a username (like `PeterNaudus`) or an email address (like `peter.naudus@example.com`)

```
git log --author=PeterNaudus
```
