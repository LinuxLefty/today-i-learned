# How to Switch a House to Generator Power

## For Generator Power

1. Start generator
1. Hook up cord from generator to inlet box
1. Turn breaker on generator to on position
1. Go to main panel
1. Turn off all individual breakers
1. Turn off main breaker
1. Side up interlock and turn on generator breaker
1. Tun on individual breakers you want to run on generator
1. If generator slows down then there are too many circuits on at the same time

## For Utility Power

1. Turn off individual breakers
1. Turn off generator breaker
1. Slide interlock down and turn main breaker on
1. Turn on all individual breakers
1. Go to generator
1. Turn breaker on generator to off position
1. Unplug cord from generator to inlet box
1. Turn generator off
