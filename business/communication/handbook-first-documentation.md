# Handbook-First Documentation

A handbook-first approach to documentation creates a single source of truth.

Read: https://about.gitlab.com/company/culture/all-remote/handbook-first-documentation/

See also: https://about.gitlab.com/handbook/communication/
