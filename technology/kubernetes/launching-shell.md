# Launching a Shell in Kubernetes

To launch a SSH shell to a kubernetes pod, run the following command:

```
kubectl exec -it [pod] -c [container] -- /bin/bash
```
