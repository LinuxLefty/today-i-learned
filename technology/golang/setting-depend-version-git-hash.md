# How to set dependency to a Git Hash

> Just `go get` at the commit hash you want:
> 
>     go get github.com/someone/some_module@af044c0995fe
> 
> `go get` will correctly update the dependency files (go.mod, go.sum).
> 
> More information: https://github.com/golang/go/wiki/Modules#how-to-upgrade-and-downgrade-dependencies

Source: https://stackoverflow.com/questions/53682247/how-to-point-go-module-dependency-in-go-mod-to-a-latest-commit-in-a-repo
