# Listing Information about Relations

Relations (Postgresql's term for Tables) can be listed by using the following query:

```sql
\dt
```

A relation's columns and associated types can be listed with:

```sql
\d+ (<relation-name>)
```
